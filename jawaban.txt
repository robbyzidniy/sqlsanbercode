1. membuat database myshop

	-> create database myshop_robby;

2. membuat table di dalam database

	==Table Users==
	-> create table users ( id int auto_increment, 
			name varchar (255),
			email varchar (255),
			password varchar (255),
			primary key (id));

	==Table Categories==
	-> create table categories ( id int auto_increment,
				name varchar (255),
				primary key (id));

	==Table Items==
	-> create table items ( id int auto_increment,
			name varchar(255),
			description varchar (255),
			price int,
			stock int,
			category_id int,
			primary key (id),
			foreign key (category_id) references categories (id));

3. memasukan data pada table

	===Table Users===
	-> insert into users (id, name, email, password)
    	-> values (1, "John Doe", "john@doe.com", "john123"),
    	-> (2, "Jane Doe", "jane@doe.com", "jenita123");

    	select * from users;

	===Table Categories===
	-> insert into categories (id, name)
    	-> values ( 1, "gadget"), ( 2, "clotch"), ( 3, "men"),
    	-> ( 4, "women"), ( 5, "branded");

    	select * from categories;

	===Table Items===
	-> insert into items (id, name, description, price, stock, category_id)
   	-> values (1, "Samsung B50", "Hape keren dari merek sumsang", 4000000, 100, 1),
    	-> (2, "Uniklooh", "Baju keren dari brand ternama", 500000, 50, 2),
    	-> (3, "IMHO Watch", "Jam tangan anak yang jujur banget", 2000000, 10, 1);

    select * from items;

4. a. mengambil data users
	-> select id, name, email from users;

   b. mengambil data items
	-> select * from items where price > 1000000;
	-> select * from items where name like 'unik%'; 

   c. menampilkan data items join dengan kategori
	-> select items.name, items.description, items.price,
    	 items.stock, items.category_id, categories.name from items
    	 inner join categories on items.id = categories.id;

5. mengubah data dari database
	-> update items set price = 2500000 where id = 1;
			